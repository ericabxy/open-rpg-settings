These are world and setting ideas that are appropriate for role-playing
game campaigns. They are intended to be shared universes each, so any
copyrightable material should be released with a CC-BY or CC0 license. Also
included are sourcebooks for the role-playing game rule systems that could be
used.
